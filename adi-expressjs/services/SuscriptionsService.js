var SuscriptionsDAO = require('../models/Suscriptions');
var jwt = require('jwt-simple');
var moment = require('moment');
var conf = require('../config');

var SuscriptionsService = SuscriptionsService || {};

SuscriptionsService.getUserSuscriptions = function (token, callbackOk, callbackError) { 
    
   var decoded = jwt.decode(token, conf.jwtconfig.secret);
   var loginUsuario = decoded.login;

   SuscriptionsDAO.getUserSuscriptions(loginUsuario, callbackOk, callbackError);
}

SuscriptionsService.getSuscripctionAudios = function (token, subId, pagOptions, callbackOk, callbackError) { 
    
   var decoded = jwt.decode(token, conf.jwtconfig.secret);
   var loginUsuario = decoded.login;

   SuscriptionsDAO.getSuscriptionAudios(loginUsuario, subId, pagOptions, callbackOk, callbackError);
}


SuscriptionsService.addSuscripction = function (token, subId, callbackOk, callbackError) { 
    
   var decoded = jwt.decode(token, conf.jwtconfig.secret);
   var loginUsuario = decoded.login;


   // En versión final, debemos hacer una petición a la URL que nos 
   // devolverá un XML que tenemos que leer para extraer los datos del proveedor
   // y sus audios
   // Ejemplo: http://www.ivoox.com/podcast-la-orbita-de-endor_fg_f113302_filtro_1.xml

   // - Añadir datos del proveedor a la tabla [feeds]
   // - Añadir datos datos de los audios que aparece en el xml a la tabla [audios]
   // - Añadir la suscripción del usuario loginUsuario al nuevo feed que hemos introducido

   callbackOk();
}



module.exports = SuscriptionsService;