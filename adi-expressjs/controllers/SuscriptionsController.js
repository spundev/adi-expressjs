var SuscriptionsService = require('../services/SuscriptionsService');

exports.suscriptions = function(app) {

    // GET /api/suscriptions
    // Obtener las suscripciones del usuario
    app.get('/api/suscriptions', function(req, res, next) {

		var rawToken = req.headers['authorization'];;
		if(rawToken) {
			var token = rawToken.split("Bearer ")[1];

			SuscriptionsService.getUserSuscriptions(token, function(results) {
	        	res.status(200);
                res.send(results); 
	        }, function(err) {
				// Si ha fallado
                res.status(500);    
                res.send({"error": err}); 
	        });

		} else {
			// Si la peticón no está bien formada
            res.status(400)
            res.send({"error": "token no proporcionado"});
		}
    });


    // GET /api/suscriptions/:subId/audios
    // Obtener los audios de una suscripción
    app.get('/api/suscriptions/:subId/audios', function(req, res, next) {

    	 // Paginación
	 	var paginationOptions = {
	 		// Limite de resultados que queremos obtener
	 		limit: req.query.limit,
	 		// Valor de la la útlima id que recibimos y 
	 		// a partir de la cual queremos resultados en esta consulta
	 		lastId: req.query.lastId
	 	}

		var rawToken = req.headers['authorization'];
		var subId = req.params.subId;

		if(rawToken && subId) {
			var token = rawToken.split("Bearer ")[1];

			SuscriptionsService.getSuscripctionAudios(token, subId, paginationOptions, function(results) {
	        	res.status(200);
                res.send(results); 
	        }, function(err) {
				// Si ha fallado
                res.status(500);    
                res.send({"error": err}); 
	        });

		} else {
			// Si la peticón no está bien formada
            res.status(400)
            res.send({"error": "token no proporcionado"});
		}
    });


    // POST /api/suscriptions [mockup]
    // Añadir suscripción al usuario
    app.post('/api/suscriptions', function(req, res, next) {

    	// Recibimos una url 
		var feedURL = req.body['feedUrl'];

		var rawToken = req.headers['authorization'];

		if(rawToken && feedURL) {

			// Comprobamos si es una URL bien formada
			var regex = new RegExp("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");
			if(regex.test(feedURL)){

				// Si es una URL correcta
				var token = rawToken.split("Bearer ")[1];

				SuscriptionsService.addSuscripction(token, feedURL, function() {
		        	res.status(201);
	                res.send(); 
		        }, function(err) {
					// Si ha fallado
	                res.status(500);    
	                res.send({"error": err}); 
		        });

	        } else { 
	        	// Si no es una URL valida
			  	res.status(400)
            	res.send({"error": "feedUrl no valida"});
			}

		} else {
			// Si la peticón no está bien formada
            res.status(400)
            res.send({"error": "la petición no está bien formada"});
		}
    });

};
