var db = require('../db');

var Users = Users || {};

// Crea un nuevo usuario en la BD
Users.crear = function (name, password, callbackOk, callbackError) {  

	var values = [name, password];

	db.query('INSERT INTO users (user_login, user_pass) VALUES(?, ?)', values, function(err, result) {
		if (err) {
			callbackError(err);
		} else {
    		callbackOk();
		}
	});
};


// Busca un usuario en la BD
Users.buscarUsuario = function (name, password, callbackOk, callbackError) {  

	var values = [name, password];

	db.query('SELECT * FROM users WHERE user_login = ? AND user_pass = ?', values, function(err, rows, fields) {
		if (err) {
			callbackError(err);
		} else {
    		callbackOk(rows);
		}
	});
};


// Borra un usuario de la BD
Users.borrar = function (loginUsuario, callbackOk, callbackError) {  

	db.query('DELETE FROM users WHERE user_login = ?', loginUsuario, function(err, rows, fields) {
		if (err) {
			callbackError(err);
		} else {
    		callbackOk(rows);
		}
	});
};


// Edita un usuario de la BD
Users.editar = function (loginUsuario, nuevoLogin, nuevoPass, callbackOk, callbackError) {  

	var values = [nuevoLogin, nuevoPass, loginUsuario];

	db.query('UPDATE users SET user_login = ?, user_pass = ? WHERE user_login = ?;', values, function(err, rows, fields) {
		if (err) {
			callbackError(err);
		} else {
    		callbackOk(rows);
		}
	});
};

module.exports = Users;