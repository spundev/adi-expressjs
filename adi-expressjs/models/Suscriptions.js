var db = require('../db');

var Suscriptions = Suscriptions || {};

Suscriptions.getUserSuscriptions = function (userName, callbackOk, callbackError) {  

	db.query('SELECT sub_id \'susctiption_id\', feed_name \'name\', feed_web_url \'web_url\', feed_data_url \'data_url\', feed_last_check \'last_check\', feed_avg_refresh_time \'avg_refresh_time\', feed_last_guid \'last_guid\' ' +
			'FROM users us, suscriptions sub, feeds fds ' +
			'WHERE us.user_id = sub.sub_user_id ' +
			'AND sub.sub_feed_id = fds.feed_id ' +
			'AND us.user_login = ? ', userName, function(err, rows, fields) {

		if (err) {
			callbackError(err);
		} else {
    		callbackOk(rows);
		} 
	});
};


Suscriptions.getSuscriptionAudios = function (userLogin, suscriptionId, pagOptions, callbackOk, callbackError) {  

	var values = [userLogin, suscriptionId];

	var query = 'SELECT aud.* ' +
			'FROM users us, suscriptions sub, feeds fds, audios aud ' +
			'WHERE us.user_id = sub.sub_user_id ' +
			'AND sub.sub_feed_id = fds.feed_id ' +
			'AND fds.feed_id = aud.audio_feed ' +
			'AND us.user_login = ? ' +
			'AND sub.sub_id = ? ';


	// Páginación
	if(pagOptions.lastId) {
		query += 'AND aud.audio_id > ? ';
		values.push(pagOptions.lastId);
	}

	if(pagOptions.limit) {
		query += ' LIMIT ' + pagOptions.limit;
	}

	db.query(query, values, function(err, rows, fields) {
				
		if (err) {
			callbackError(err);
		} else {
    		callbackOk(rows);
		}  
	});
};


module.exports = Suscriptions;