var express = require('express');
var bodyParser = require('body-parser');
var env = process.env;

var app = express();

// parse application/json
app.use(bodyParser.json())

app.get('/', function (req, res) {
	res.sendFile(__dirname + '/public/index.html');
});


// RESTfull
// Usuarios
require('./controllers/UsuarioController.js').usuario(app);
// Suscripciones
require('./controllers/SuscriptionsController.js').suscriptions(app);
// Audios
require('./controllers/AudiosController.js').audios(app);
// Discover
require('./controllers/DiscoverController.js').discover(app);


var port = env.NODE_PORT || 3000;
var ip = env.NODE_IP;
app.listen(port, ip, function() {
    console.log('Listening on port ' + port + '...');
});


module.exports = app;


