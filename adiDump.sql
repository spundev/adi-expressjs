CREATE DATABASE  IF NOT EXISTS `adi` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `adi`;
-- MySQL dump 10.13  Distrib 5.5.35, for debian-linux-gnu (x86_64)
--
-- Host: 127.0.0.1    Database: adi
-- ------------------------------------------------------
-- Server version	5.5.35-1ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `audios`
--

DROP TABLE IF EXISTS `audios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `audios` (
  `audio_id` int(11) NOT NULL AUTO_INCREMENT,
  `audio_feed` int(11) NOT NULL,
  `audio_author` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `audio_origlink` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `audio_title` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `audio_content` text CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `audio_url` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `audio_guid` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `audio_pubdate` datetime NOT NULL,
  `audio_fetchdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`audio_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='Audios';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `audios`
--

LOCK TABLES `audios` WRITE;
/*!40000 ALTER TABLE `audios` DISABLE KEYS */;
INSERT INTO `audios` VALUES (1,1,'LA ÓRBITA DE ENDOR','origlink','LODE 7x07 DOCTOR STRANGE (Doctor Extraño)','LODE 7x07 DOCTOR STRANGE (Doctor Extraño)','http://podcastcdn-16.ivoox.com/audio/2/9/5/9/1477955011_lode7x07doctorstrangedoctorextrano-orbitadeendor-ivoox13549592.mp3?secure=YTvQcmXYFEreTd1MP6JoIA==,1478541485','asd','2016-10-31 00:00:00','0000-00-00 00:00:00'),(2,1,'LA ÓRBITA DE ENDOR','origlink','LODE 7x06 especial RAMBO audio completo -REPARADO-','Si la semana pasada el que se pasaba por La Órbita de Endor era Arnold Schwarzenegger, esta vez es nada menos que Sylvester Stallone el que nos ameniza el día con su saga RAMBO. Monográfico dedicado al ex boina verde que llevaba la guerra en las venas, que hizo del infierno su hogar y que nos entregó horas y horas de diversión. Desde la novela donde fue concebido, pasando por la adaptación donde fue acorralado y llegando, en un repaso exhaustivo por toda la saga, hasta su última aventura que culmina en el buzón de su padre, hoy sólo tenemos palabras para John Rambo. Buenas y alguna mala. ¡A descargar!','https://ia801507.us.archive.org/31/items/LODE7x06RAMBOCompletob/LODE%207x06%20RAMBO%20completob.mp3','','2016-10-26 00:00:00','2016-11-07 16:01:50'),(3,1,'LA ÓRBITA DE ENDOR','','LODE 7x05 DESAFÍO TOTAL libro + película, Cómo se hace un videojuego','Arnold Schwarzenegger vuelve a La Órbita de Endor, y como siempre que eso ocurre, todos nos ponemos firmes. Si viene acompañado de Philip K Dick y del bueno de Paul Verhoeven, aún más. DESAFÍO TOTAL, es lo que toca. Libro + Película. En este caso, un relato titulado PODEMOS RECORDARLO POR USTED, y el film que se toma la vía libre para presentarnos una historia de aventuras en un futuro improbable, con toda la incorreción y salvajismo de una época en la que el cine era más irreverente. Nos consta que hay un remake de la película, con Colin Farrel entre otros. Pero éste no es su programa. Jaime Angulo y Pako Garrido estarán con el director del programa analizando estos clásicos. Después, Rafa Martínez y Antonio Runa entrevistarán a Ricard Pillosu programador y diseñador de videojuegos que ha trabajado para compañías de gran renombre, entre ellas Pyro Studios o Crytech, para que nos hable paso a paso sobre el proceso de realización de un videojuego. Tan interesante, que aunque los videojuegos no sean lo vuestro, os fascinará. Y si eres gamer, sencillamente fliparás con el dossier de hoy.','https://ia801509.us.archive.org/32/items/Lode7x05DesafioTotalComosehacenvideojuegos/Lode7x05DesafoTotalCmoSeHaceUnVideojuego.mp3','','2016-10-19 00:00:00','2016-11-07 17:03:31'),(4,1,'LA ÓRBITA DE ENDOR','','ALTERNATIVA LODER 19 \'ROGUE ONE trailer final\' 17 octubre 2016','Hoy en ALTERNATIVA LODER, análisis concienzudo del trailer final de ROGUE ONE y alguna pildorita más para acompañar.','https://ia801506.us.archive.org/18/items/Alternativalode17octubre2016Trailerrogueone/AlternativaLoder17Octubre2016TrailerRogueOne.mp3','','2016-10-17 00:00:00','2016-11-07 17:03:31'),(5,1,'LA ÓRBITA DE ENDOR','','LODE 7x04 EL SECRETO DE LA PIRÁMIDE, ELVIEJO LOGAN','La Órbita de Endor se pone sus mejores galas para recibir al mejor detective de todos los tiempos, en esta ocasión en su faceta más juvenil. Young Sherlock Holmes o como se tituló en España, EL SECRETO DE LA PIRÁMIDE será analizada como es debido en nuestra Videoteca Lode de hoy. Jaime Angulo y Antonio Runa esclarecerán todos los misterios que entraña este clásico de los ochenta. Y en la sección de cómics, Entre Viñetas, junto a Rafa Pajis vamos a recorrer los arrasados Estados Unidos de un futuro hipotético, al lado de un Lobezno deprimido y un Ojo de Halcón ciego en la historia de Mark Millar y Steve McNiven EL VIEJO LOGAN. Es lo que hay, ahora tú decides.','https://ia801506.us.archive.org/1/items/Lode7x04ElSecretoDeLaPiramideElViejoLogan/Lode7x04ElsecretodelapiramideElviejologan.mp3','','2016-10-11 00:00:00','2016-11-07 17:03:38'),(6,3,'Android Developers','','Episode 57: Espresso Test Recorder','In this episode, Chet and Tor talk with Stas Negara from the Firebase team about the Espresso Test Recorder, a feature in Android Studio which lets you easily record Android UI tests.','http://www.noiseaddicts.com/samples_1w72b820/3903.mp3','','2016-10-21 00:00:00','2016-11-07 17:09:00'),(7,3,'Android Developers','','Episode 56: In The Beginning','In this episode, Chet and Tor talk with Ficus Kirkpatrick about the early days of Android.','http://www.noiseaddicts.com/samples_1w72b820/3903.mp3','','2016-10-06 00:00:00','2016-11-07 17:09:00'),(8,3,'Android Developers','','Episode 55: Glide','In this episode, Chet and Romain talk with Sam Judd from the Google Photos team about Glide, an image loading and caching library, and its use in the Photos app.','http://www.noiseaddicts.com/samples_1w72b820/3903.mp3','','2016-08-31 00:00:00','2016-11-07 17:09:00'),(9,3,'Android Developers','','Episode 54: AAPT','In this episode, Chet and Tor talk with Adam Lesinski from the Android framework team. Adam works on aapt (Android Asset Packaging Tool), which compiles the resources used by the framework and for apps. Listen in to learn more about how it works, as well as some of the optional features that aapt provides that you could take advantage of. He also might talk about some of the future development going on for more robust and performant tools. Maybe.','http://www.noiseaddicts.com/samples_1w72b820/3903.mp3','','2016-08-11 00:00:00','2016-11-07 17:09:00'),(10,2,'Dan Carlin','','Show 58 - Kings of Kings III','If this were a movie, the events and cameos would be too numerous and star-studded to mention. It includes Xerxes, Spartans, Immortals, Alexander the Great, scythed chariots, and several of the greatest battles in history.','http://www.noiseaddicts.com/samples_1w72b820/3903.mp3','','2016-08-07 00:00:00','2016-11-07 17:11:22'),(11,2,'Dan Carlin','','Show 57 - Kings of Kings II','From Biblical-era coup conspiracies to the horrific aftermath of ancient combat this second installment of the series on the Kings of Achaemenid Persia goes where only Dan can take it. For better or worse…','http://www.noiseaddicts.com/samples_1w72b820/3903.mp3','','2016-03-20 00:00:00','2016-11-07 17:11:22'),(12,2,'Dan Carlin','','Show 56 - Kings of Kings','Often relegated to the role of slavish cannon fodder for Sparta\'s spears, the Achaemenid Persian empire had a glorious heritage. Under a single king they created the greatest empire the world had ever seen.','http://www.noiseaddicts.com/samples_1w72b820/3903.mp3','','2015-10-29 00:00:00','2016-11-07 17:11:22'),(13,4,'Star Wars, Rogue One, The Force Awakens, Oxygen, Rebels, Clone Wars, Jedi, Sith, Empire, TV, Film','','Star Wars Oxygen: Vol. 36','We discuss the final tracks of the Revenge of the Sith soundtrack including the dramatic conclusion of the film and the extended End Credits.. Plus David reveals the top 10 most used tracks from the entire saga and more.','http://www.noiseaddicts.com/samples_1w72b820/3903.mp3','','2016-11-05 00:00:00','2016-11-07 17:13:15'),(14,4,'Star Wars, Rogue One, The Force Awakens, Oxygen, Rebels, Clone Wars, Jedi, Sith, Empire, TV, Film','','Rebel Force Radio: October 28, 2016','We are scarier than usual here at RFR as we welcome Halloween to the STAR WARS universe. Legendary STAR WARS author and pillar of the fan community, our bud Steve Sansweet joins us IN THE CANTINA to talk about Rancho Obi-Wan, ROGUE ONE, and more. We review the latest ROGUE ONE commercial from Duracell and look for clues about the upcoming film. News headlines include the recent Donald Glover Lando casting announcement, a Daisy Ridley update, and info about THE FORCE AWAKENS 3D BluRay release, along with discussion about Rey’s TFA “Forceback”. Plus, an opportunity for you to win an incredible STAR WARS phone case from Tantrum Cases, listener voice mail and much more.','http://www.noiseaddicts.com/samples_1w72b820/3903.mp3','','2016-10-28 00:00:00','2016-11-07 17:13:15'),(15,4,'Star Wars, Rogue One, The Force Awakens, Oxygen, Rebels, Clone Wars, Jedi, Sith, Empire, TV, Film','','Star Wars Rebels: Declassified S3E6','It\'s a showdown between a Separatist Tactical Battle Droid, a leader of the Republic Clone Army, and pair of Jedi warriors. What\'s this? An episode of Star Wars: The Clone Wars? Not quite, but this episode of STAR WARS REBELS provides a bit of closure to the famed galactic Clone Wars when an isolated battalion of battle droids are discovered by Kanan, Ezra, and Captain Rex igniting \"The Last Battle\". Joining Jimmy, Jason and Spencer at the roundtable is Jovial Jay Shepard from Jedi Journals and the Random Chatter Podcast Network. ','http://www.noiseaddicts.com/samples_1w72b820/3903.mp3','','2016-10-27 00:00:00','2016-11-07 17:13:15'),(16,4,'Star Wars, Rogue One, The Force Awakens, Oxygen, Rebels, Clone Wars, Jedi, Sith, Empire, TV, Film','','Rebel Force Radio: October 21, 2016','As we continue our review of the final full trailer for ROGUE ONE, we turn the spotlight onto the RFR audience by featuring your voice mails and feedback. We hear and comment on your thoughts, opinions, expectations and speculation about the upcoming STAR WARS spin-off film. Plus, more rumors and reports from the ROGUE ONE reshoots and post-production, we listen to the music from the trailer, and we argue about Darth Vader\'s red lenses. ','http://www.noiseaddicts.com/samples_1w72b820/3903.mp3','','2016-10-21 00:00:00','2016-11-07 17:13:15');
/*!40000 ALTER TABLE `audios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `feeds`
--

DROP TABLE IF EXISTS `feeds`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feeds` (
  `feed_id` int(11) NOT NULL AUTO_INCREMENT,
  `feed_name` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `feed_web_url` varchar(500) CHARACTER SET utf8 NOT NULL,
  `feed_data_url` varchar(500) CHARACTER SET utf8 NOT NULL,
  `feed_last_check` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `feed_avg_refresh_time` int(11) NOT NULL,
  `feed_last_guid` varchar(500) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`feed_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Feeds de audios';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feeds`
--

LOCK TABLES `feeds` WRITE;
/*!40000 ALTER TABLE `feeds` DISABLE KEYS */;
INSERT INTO `feeds` VALUES (1,'Podcast La Órbita De Endor','','http://www.ivoox.com/podcast-podcast-la-orbita-de-endor_sq_f113302_1.html','2016-11-07 16:53:22',0,'0'),(2,'Dan Carlin\'s Hardcore History','http://www.dancarlin.com/','http://feeds.feedburner.com/dancarlin/history?format=xml','2016-11-07 16:53:22',0,'Hardcore history'),(3,'Android Developers Backstage','http://androidbackstage.blogspot.com/','http://androidbackstage.blogspot.com/feeds/posts/default?alt=rss','2016-11-07 16:55:40',0,''),(4,'Rebel Force Radio: Star Wars Podcast','http://rebelforceradio.com/','http://rebelforceradio.libsyn.com//rss','2016-11-07 16:55:40',0,'');
/*!40000 ALTER TABLE `feeds` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `listen`
--

DROP TABLE IF EXISTS `listen`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `listen` (
  `listen_id` int(11) NOT NULL AUTO_INCREMENT,
  `listen_user_id` int(11) NOT NULL,
  `listen_audio_id` int(11) NOT NULL,
  `listen_status` enum('complete','inprogress') COLLATE utf8_spanish_ci NOT NULL,
  `listen_time` double NOT NULL,
  PRIMARY KEY (`listen_user_id`,`listen_audio_id`),
  UNIQUE KEY `listen_id_UNIQUE` (`listen_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de escuchas';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listen`
--

LOCK TABLES `listen` WRITE;
/*!40000 ALTER TABLE `listen` DISABLE KEYS */;
INSERT INTO `listen` VALUES (12,1,3,'inprogress',3000);
/*!40000 ALTER TABLE `listen` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suscriptions`
--

DROP TABLE IF EXISTS `suscriptions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `suscriptions` (
  `sub_id` int(11) NOT NULL AUTO_INCREMENT,
  `sub_user_id` int(11) NOT NULL,
  `sub_feed_id` int(11) NOT NULL,
  `sub_feed_rename` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`sub_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de suscripciones';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suscriptions`
--

LOCK TABLES `suscriptions` WRITE;
/*!40000 ALTER TABLE `suscriptions` DISABLE KEYS */;
INSERT INTO `suscriptions` VALUES (1,1,1,''),(2,1,2,''),(3,1,3,''),(4,2,2,''),(5,2,4,'');
/*!40000 ALTER TABLE `suscriptions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_login` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `user_pass` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`user_id`,`user_login`),
  UNIQUE KEY `user_login_UNIQUE` (`user_login`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci COMMENT='Tabla de usuarios de la aplicación';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'vnm','vnm'),(2,'adi','adi'),(26,'nuevo','nuevo');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-11-07 23:15:54
